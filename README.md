# danaxa

By clicking on each tool, hand mode would be disabled and the clicked tool will be active. 
To navigate between tools you can use shortkeys as following:

shortKey: 1 => 'Screen Mode'
shortKey: 2 => 'Ball Pen'
shortKey: 3 => 'Magic Wand'
shortKey: 4 => 'Brush'
shortKey: 5 => 'Eraser'
shortKey: 6 => 'Map Pin'
shortKey: 7 => 'Frame'

Also, by pressing Esc key, the hand mode button would be active and other tools would be disabled.